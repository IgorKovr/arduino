 #include "SIM900.h"
#include "RTClib.h"
#include <SoftwareSerial.h>
#include <SD.h>
#include <Wire.h>
#include "inetGSM.h"
#include "DHT.h"
#include <Time.h>
#include <TimerOne.h>

/*   MOISTURE
  # sensor value description
  # 700 - 560 - super dry
  # 450 - contacts touch the water
  # 400, 340, 300, 280, 250, 230, 200 - 0.5 cm iteration (0 - 3.5 cm)
  # 185 completely in water
*/

#define MOISTURE_PIN A0
#define PROTOCELL_PIN A15     // the cell and 10K pulldown are connected to a0

float  deepInWater = 1;

//  DHT 
//#define DHTPIN A0 
#define DHTPIN A14
#define DHTTYPE 11   // DHT 11 

#define SIM900_POWER D9

//Relay 
#define RELAY_WATER  47 

InetGSM inet;
RTC_DS1307 rtc;
DHT dht(DHTPIN, DHTTYPE);
#include "sms.h"
char sendNumber[]="0932287117";
//#define SMS_ON   "123"
//#define SMS_OFF  "321"
#define SMS_TEMP "?"
char messageRead[180];
char pos;
SMSGSM sms;
char smsFromNumber[30];
char *p;
boolean rtcWorking = false;
boolean sdWorking = false;
boolean inetWorking = false;

float temp;
float *humidity;
char responce[250];

int timeM;
int timeH;
int timeS;
int timeYear;
int timeMonth;
int timeDay;
const int utc_diff = -2;

int waterOnSec = 45;
int waterOffSec = 60*4;
int curWaterSec = 0;

int waterOnMin = 1;
int waterOffMin = 4;
int curWaterMin = 0;

boolean waterOn = true;


void setup()
{
    Serial.begin(9600);
    
    // Light sensor power
    
    digitalWrite(41, LOW);       // turn on high (+5V) 
    digitalWrite(40, HIGH);       // turn on high (+5V)  
    
    // DHT Temp power
    
    pinMode(40, OUTPUT);
    pinMode(41, OUTPUT);
    digitalWrite(41, LOW);       // turn on high (+5V) 
    digitalWrite(40, HIGH);       // turn on high (+5V)        

    
// Relay setup
  pinMode(RELAY_WATER, OUTPUT);    
    
// gsm setup
    Serial.println("Initializing GSM...");
    boolean started=false;    
    if (gsm.begin(4800)){
      Serial.println("\nstatus=READY");
      started=true;  
    }
    else 
      Serial.println("\nstatus=IDLE");
 // inet Setup
    if(started){
      if (inet.attachGPRS("internet", "", ""))
        Serial.println("internet=ATTACHED");
      else 
        Serial.println("internet=ERROR");
    }
    
// SD setup
    Serial.print("Initializing SD card...");
     pinMode(53, OUTPUT);    
      if (!SD.begin(53)) {
        Serial.println("initialization SD failed.");
        sdWorking = false; 
        }
      else {
        Serial.println("initialization SD done.");
         sdWorking = true; 
      }
      
   
  
// RealTimeClock Setup
  
    #ifdef AVR
      Wire.begin();
    #else
      Wire1.begin(); // Shield I2C pins connect to alt I2C bus on Arduino Due
    #endif
      rtc.begin();
      
      // Set +5V pin
//    pinMode(49, OUTPUT);           // set pin to output
//    digitalWrite(49, HIGH);       // turn on high (+5V) 

    // following line sets the RTC to the date & time this sketch was compiled  
    rtc.adjust(DateTime(__DATE__, __TIME__));

    if (! rtc.isrunning()) {
      Serial.println("RTC is NOT running!");
      rtcWorking = false;
    }
    else  {
      rtcWorking = true;
      Serial.println("initialization RTC done.");
    }
    
    
    // Time 
//    setTime(8,29,0,1,1,11); // set time to Saturday 8:29:00am Jan 1 2011
   setTime(23,57,0,1,1,14);
   
   // Timer Interupt
  Timer1.initialize(1000000);
  Timer1.attachInterrupt(oneSecondTimer); // blinkLED to run every 0.15 seconds
   
};
 
void loop()
 
{
   float h;
   humidity = &h;
   temp = readSensor(humidity);
  
  int t1 = temp;
  int h1 = *humidity;
   
   getTime();
   
 if (gsm.begin(4800)){
     if (inet.attachGPRS("internet", "", "")){
       sendData(t1, h1);
    } 
 } else {
    Serial.println("something wrong with sim900");  
 }
 
//  sendData(t1, h1);
     gsm.WhileSimpleReadToStr(responce);
     Serial.println(responce);
//    delay(60000);
   checkMessage();
};


void getTime(){
  DateTime now = rtc.now();
  // calculate utc time
  DateTime utcTime (now.unixtime() + utc_diff*(3600));
  timeM = utcTime.minute();
  timeH = utcTime.hour();
  timeS = utcTime.second();
  timeYear  = utcTime.year();
  timeMonth = utcTime.month();
  timeDay   = utcTime.day();
};

void sendData(int temp, int humidity){
  Serial.println("Sending Data...");
  
  String tempStr;
  
  if (rtcWorking){
  
    char chYear[5];
    char chMonth[3];
    char chDay[3];
    char chMinutes[3];
    char chHour[3];
    char chSec[3];
    
    itoa(timeYear,chYear,10);
  //  formatTimeDigits(chYear, timeYear);
    formatTimeDigits(chMonth, timeMonth);
    formatTimeDigits(chDay,   timeDay);
    formatTimeDigits(chMinutes, timeM);
    formatTimeDigits(chHour, timeH);
    formatTimeDigits(chSec,  timeS);
    
    tempStr = "temp,";
    tempStr = tempStr + chYear + "-" + chMonth + "-"+ chDay + "T" + chHour + ":" + chMinutes + ":" + chSec + "Z," + temp + "\n";
    tempStr = tempStr + "humidity,"  + chYear  + "-"+ chMonth + "-"+ chDay + "T"  + chHour + ":" + chMinutes + ":"+ chSec + "Z," + humidity + "\n";
    tempStr = tempStr + "moisture," + moistureLevel()  + "\n";
    tempStr = tempStr + "Watering," + waterOn  + "   ";
  } else {
    Serial.println("Sending data with no time");
    tempStr = "temp,";
    tempStr = tempStr + temp + "\n";
    tempStr = tempStr + "humidity," + humidity + "\n";
    tempStr = tempStr + "moisture," + moistureLevel() + "\n";
    tempStr = tempStr + "light,"    + lightSensor()   + "\n";
    tempStr = tempStr + "Watering," + waterOn  + "   ";
    
  }
  
    char sendStr[tempStr.length()]; 
    tempStr.toCharArray(sendStr, tempStr.length());
    Serial.println(tempStr);
     inet.httpPUT("api.xively.com",     80, "/v2/feeds/1799206591.csv", sendStr, "", responce, 50);
}

float readSensor(float *humid){
  // Read moisture level
  moistureLevel();
  // Reading temperature or humidity takes about 250 milliseconds
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  temp = t;
  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h))
    Serial.println("Failed to read from DHT");
  *humid = h;
  return t; // Warning float to int cast
}

void formatTimeDigits(char strOut[], int num)
{
    strOut[0] = '0' + (num / 10);
    strOut[1] = '0' + (num % 10);
    strOut[2] = '\0';
}

// Sms Work

void checkMessage(void){
   pos=sms.IsSMSPresent(SMS_ALL);
 if (!pos){
//    Serial.println("No messages\n\n");
  }
 else {
   Serial.print("Message found!\nOn position:");
   Serial.println((int)pos);
   Serial.println("Message text:");
   messageRead[0]='\0';
   sms.GetSMS((int)pos,smsFromNumber,messageRead,180);
   Serial.println(messageRead);
   checkSendSMSWithTemp(messageRead);
   checkSetWatterCycle(messageRead);
   sms.DeleteSMS((int)pos); // Deleting founded message
 }
}

// SMS Work

void checkSendSMSWithTemp(char *messageToCheck){
     p=strstr(messageToCheck, SMS_TEMP);
     if(p){       
      char messageToSend[120 + 1];  //buffer used to format a line (+1 is for trailing 0)
      int t = temp;
      int h = *humidity;
      sprintf(messageToSend,"Hi, I'm ok!\n Temperature is: +%d*C\nHumidity: %d%%",t, h);   
      Serial.println("Sending Message:");
      Serial.println(messageToSend);
//       char message[ ] = "Temp:";
       sms.SendSMS(smsFromNumber,messageToSend);
     }
}

void checkSetWatterCycle(char *messageToCheck){
  p=strstr(messageToCheck, "Wattering");
  if(p){   
      String str = messageToCheck;
      int firstDigit;
      int lastDigit;
      
      int firstBracket = str.indexOf('/');
      int secondBracket = str.indexOf('/', firstBracket+1);
      if (firstBracket == -1 || secondBracket == -1 ){
        Serial.println("Wrong wattering params 1dgt.");
        return;
      }
      
      String digit = str.substring(firstBracket, secondBracket);
      
      int firstDidgit = digit.toInt();
      
      firstBracket = secondBracket;
      secondBracket = str.indexOf('/', firstBracket+1);
      if (firstBracket == -1 || secondBracket == -1 ){
        Serial.println("Wrong wattering params 2dgt.");
        return;
      }      
      
      digit = str.substring(firstBracket, secondBracket);
      int secondDigit = digit.toInt();
      
       Serial.println("Wattering on min:");
       Serial.println(firstDidgit);
       Serial.println("Wattering off min:");      
       Serial.println(secondDigit);
     }
}

int moistureLevel(){
 return analogRead(MOISTURE_PIN);
}


int lightSensor() {
  int photocellReading = analogRead(PROTOCELL_PIN);  
 
  Serial.print("Light = ");
  Serial.print(photocellReading);     // the raw analog reading
 
  // We'll have a few threshholds, qualitatively determined
  if (photocellReading < 10) {
    Serial.println(" - Dark");
  } else if (photocellReading < 200) {
    Serial.println(" - Dim");
  } else if (photocellReading < 500) {
    Serial.println(" - Light");
  } else if (photocellReading < 800) {
    Serial.println(" - Bright");
  } else {
    Serial.println(" - Very bright");
  }
  
  return photocellReading;
  
}

void oneSecondTimer(){
  curWaterSec ++;
  if (waterOn){
//    Serial.println("Wattering ON");
    digitalWrite(RELAY_WATER,HIGH);           // Turns ON Watering
    if (curWaterSec >= waterOnSec){
      curWaterSec = 0;
      waterOn = false;
    }  
  } else {
    digitalWrite(RELAY_WATER,LOW);           // Turns OFF Watering
//    Serial.println("Wattering OFF");
        if (curWaterSec >= waterOffSec){
      curWaterSec = 0;
      waterOn = true;
    } 
  }
}

//void oneMinuteTimer(){
//    curWaterMin ++;
//  if (waterOn){
//    Serial.println("Wattering ON");
//    digitalWrite(RELAY_WATER,HIGH);           // Turns ON Watering
//    if (curWaterMin >= waterOnMin){
//      curWaterMin = 0;
//      waterOn = false;
//    }  
//  } else {
//    digitalWrite(RELAY_WATER,LOW);           // Turns OFF Watering
//    Serial.println("Wattering OFF");
//        if (curWaterMin >= waterOffMin){
//      curWaterMin = 0;
//      waterOn = true;
//    } 
//  }
//}
  
size_t strlen(const char *str) {
    const char *s;
    for (s = str; *s; ++s);
    return(s - str);
}
  
